public class Board{
	private Tile[][] grid;
	final int size = 3;
	public Board(){
		this.grid = new Tile[size][size];
		for(int i = 0; i < this.grid.length; i++){
			for(int j = 0; j < this.grid[i].length; j++){
				grid[i][j] = Tile.BLANK;
			}
		}
	}
	
	public String toString(){
		String builder = "";
		builder += "  0 1 2 \n";
		for(int i = 0; i < this.grid.length; i++){
			builder += i + " ";
			for(int j = 0; j < this.grid[i].length; j++){
				builder += this.grid[i][j].getName() + " ";
			}
			builder += "\n";
		}
		return builder;
	}
	
	public boolean placeToken(int row, int col, Tile playerToken){
		if((row < 0 || col < 0) && (row > this.grid.length || col > this.grid[row].length))
			return false;
		if(this.grid[row][col] == Tile.BLANK){
			this.grid[row][col] = playerToken;
			return true;
		}
		return false;
	}
	
	public boolean checkIfFull(){
		for(int i = 0; i < this.grid.length; i++){
			for(int j = 0; j < this.grid[i].length; j++){
				if(this.grid[i][j] == Tile.BLANK)
					return false;
			}
		}
		return true;
	}
	
	private boolean checkIfWinningHorizontal(Tile playerToken) {
		for (int i = 0; i < this.grid.length; i++) {
			boolean hasRow = true;
			for (int j = 0; j < this.grid[i].length; j++) {
				if (this.grid[i][j] != playerToken) {
					hasRow = false;
				}
			}
			if (hasRow) {
				return true;
			}
		}
		return false;
	}
	private boolean checkIfWinningVertical(Tile playerToken) {
    for (int j = 0; j < this.grid[0].length; j++) {
        boolean hasColumn = true;
        for (int i = 0; i < this.grid.length; i++) {
            if (this.grid[i][j] != playerToken) {
                hasColumn = false;
            }
        }
        if (hasColumn) {
            return true;
        }
    }
    return false;
}
	public void resetBoard(){
		for(int i = 0; i < this.grid.length; i++){
			for(int j = 0; j < this.grid[i].length; j++){
				this.grid[i][j] = Tile.BLANK;
			}
		}
	}
	
	public boolean checkIfWinning(Tile playerToken){
		if(checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken))
			return true;
		return false;
	}
}