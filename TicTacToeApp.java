import java.util.Scanner;
public class TicTacToeApp{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		System.out.println("hello");
		Board game = new Board();
		boolean gameOver = false;
		int player = 1;
		Tile playerToken = Tile.X;
		
		int winsP1 = 0;
		int winsP2 = 0;
		
		do{
			while(!gameOver){
				System.out.println(game);
				System.out.println("Enter your positions player " + player);
				int row = reader.nextInt();
				int col = reader.nextInt();
				
				//Player token
				if(player == 1)
					playerToken = Tile.X;
				else 
					playerToken = Tile.O;
				
				//Pos taken
				while (!game.placeToken(row, col, playerToken)) {
					System.out.println("Position already taken. Enter a new position:");
					row = reader.nextInt();
					col = reader.nextInt();
				}

				//If win
				if(game.checkIfWinning(playerToken)){
					System.out.println(game);
					System.out.println("Player " + player + " wins!");
					
					if(player == 1)
						winsP1++;
					if(player == 2)
						winsP2++;
					gameOver = true;
				} else if(game.checkIfFull()){ //If full
					System.out.println(game);
					System.out.println("It's a tie!");
					gameOver = true;
				}
				
				//Switch players
				player++;
				if(player > 2)
					player = 1;
			}
			
			reader.nextLine();
			
			System.out.println("Total wins: ");
			System.out.println("P1: " + winsP1 + " P2: " + winsP2);
			System.out.println("Continue playing? y/n");
			String answer = reader.nextLine();
			if(answer.equals("y")){
				gameOver = false;
				game.resetBoard();
			}
			
		}while (!gameOver);
	}
}